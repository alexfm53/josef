#!/bin/bash
git pull origin master
docker pull alextest321/josef:latest

docker rm josef  -f || true
echo -e '\e[1m\e[34m\nRestarting service..\e[0m\n'
#stop the application
docker run -d --rm --name josef -p 80:80 alextest321/josef:latest
#make sure close port 8080
